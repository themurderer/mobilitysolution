# Mafia

Technical Test created by Raúl Salazar

Company Meep

## Description
La idea es generar un microservicio con Spring Boot que haga polling cada 1/2
minutos en el endpoint anterior detectando cambios, es decir nuevos vehículos


## Stack
* Java 11
* Spring Boot
* Swagger
* jUnit 5

### Dependencies
* spring-boot-starter-web
* spring-boot-starter-test
* spring-boot-starter-data-mongodb
* lombok

## Configuration

The following properties can change the behavior:

| __Property__ | __Description__ | __Default value__ |
|--------------|-----------------|-------------------|
| `repository.mongo.connection.url` | Url database connection | `mongodb://localhost:27017/meep` |
| `repository.mongo.credentials.username` | Username of mongo connection| `test` |
| `repository.mongo.credentials.username` | Password of mongo connection  | `test` |
| `repository.mongo.database` | Name of default database  | `meep` |
| `external.provider.url` | Url to obtain data  | `https://apidev.meep.me/tripplan/api/v1/routers/lisboa/resources?lowerLeftLatLon=38.711046,-9.160096&upperRightLatLon=38.739429,-9.137115&companyZoneIds=545,467,473` |

## API

You can find the api associated with this domain documented in the `/spec/openapi.yml`.

## Postman
You can find the collection and environment properties associated with local in the `/postman` folder.

## Docker 
You can find the docker definition and environment properties associated with local in the `/docker` folder.

## Usage :rocket:
* To launch mongo local database:

In docker directory (/resources/docker) launch the next command: 
```bash
docker-compose up
```

* To run the service:


```bash
mvn spring-boot:run
```

Enjoy!! :fire:



