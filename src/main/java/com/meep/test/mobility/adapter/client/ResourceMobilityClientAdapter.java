package com.meep.test.mobility.adapter.client;

import com.meep.test.mobility.adapter.client.vo.ResourceMobilityClientDTO;
import com.meep.test.mobility.adapter.converter.ResourceMobilityClientDTOToResourceMobilityConverter;
import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.exception.ExternalCommunicationException;
import com.meep.test.mobility.provider.ResourceMobilityClient;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ResourceMobilityClientAdapter implements ResourceMobilityClient {

  private String externalProviderUrl;

  private final RestTemplate restTemplate;

  private final ResourceMobilityClientDTOToResourceMobilityConverter feignToDomainConverter;

  public ResourceMobilityClientAdapter(@Value("${external.provider.url}") String externalProviderUrl,
      ResourceMobilityClientDTOToResourceMobilityConverter feignToDomainConverter) {
    this.feignToDomainConverter = feignToDomainConverter;
    this.restTemplate = new RestTemplate();
    this.externalProviderUrl = externalProviderUrl;
  }

  @Override
  public List<ResourceMobility> getResources() {

    ResponseEntity<ResourceMobilityClientDTO[]> response = restTemplate.getForEntity(externalProviderUrl, ResourceMobilityClientDTO[].class);
    if (HttpStatus.OK.equals(response.getStatusCode())) {
      return Arrays.stream(response.getBody()).map(feignToDomainConverter::convert).collect(Collectors.toList());
    } else {
      throw new ExternalCommunicationException();
    }
  }
}
