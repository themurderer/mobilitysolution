package com.meep.test.mobility.adapter.converter;

import com.meep.test.mobility.adapter.client.vo.ResourceMobilityClientDTO;
import com.meep.test.mobility.domain.ResourceMobility;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ResourceMobilityClientDTOToResourceMobilityConverter implements Converter<ResourceMobilityClientDTO, ResourceMobility> {

  @Override
  public ResourceMobility convert(ResourceMobilityClientDTO clientObject) {
    return new ResourceMobility.Builder()
        .withId(clientObject.getId())
        .withBatteryLevel(clientObject.getBatteryLevel())
        .withHelmets(clientObject.getHelmets())
        .withModel(clientObject.getModel())
        .withName(clientObject.getName())
        .withRange(clientObject.getRange())
        .withX(clientObject.getX())
        .withY(clientObject.getY())
        .withCompanyZoneId(clientObject.getCompanyZoneId())
        .withLicensePlate(clientObject.getLicensePlate())
        .withResourceType(clientObject.getResourceType())
        .withResourceImageId(clientObject.getResourceImageId())
        .withRealTimeData(clientObject.getRealTimeData())
        .build();

  }
}
