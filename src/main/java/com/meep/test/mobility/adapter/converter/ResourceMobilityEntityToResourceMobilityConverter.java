package com.meep.test.mobility.adapter.converter;

import com.meep.test.mobility.adapter.repository.vo.ResourceMobilityEntity;
import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.domain.ResourceMobility.Builder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ResourceMobilityEntityToResourceMobilityConverter implements Converter<ResourceMobilityEntity, ResourceMobility> {

  @Override
  public ResourceMobility convert(ResourceMobilityEntity resourceMobilityEntity) {

    return new Builder()
        .withId(resourceMobilityEntity.getId())
        .withBatteryLevel(resourceMobilityEntity.getBatteryLevel())
        .withHelmets(resourceMobilityEntity.getHelmets())
        .withModel(resourceMobilityEntity.getModel())
        .withName(resourceMobilityEntity.getName())
        .withRange(resourceMobilityEntity.getRange())
        .withX(resourceMobilityEntity.getX())
        .withY(resourceMobilityEntity.getY())
        .withCompanyZoneId(resourceMobilityEntity.getCompanyZoneId())
        .withLicensePlate(resourceMobilityEntity.getLicensePlate())
        .withResourceType(resourceMobilityEntity.getResourceType())
        .withResourceImageId(resourceMobilityEntity.getResourceImageId())
        .withRealTimeData(resourceMobilityEntity.getRealTimeData())
        .withActive(resourceMobilityEntity.getActive())
        .withLastUpdate(resourceMobilityEntity.getLastUpdate())
        .build();
  }
}
