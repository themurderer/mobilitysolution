package com.meep.test.mobility.adapter.converter;

import com.meep.test.mobility.adapter.rest.vo.ResourceMobilityResponse;
import com.meep.test.mobility.domain.ResourceMobility;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ResourceMobilityToResourceMobilityResponseConverter implements Converter<ResourceMobility, ResourceMobilityResponse> {

  @Override
  public ResourceMobilityResponse convert(ResourceMobility resourceMobility) {

    return new ResourceMobilityResponse.Builder()
        .withId(resourceMobility.getId())
        .withBatteryLevel(resourceMobility.getBatteryLevel())
        .withHelmets(resourceMobility.getHelmets())
        .withModel(resourceMobility.getModel())
        .withName(resourceMobility.getName())
        .withRange(resourceMobility.getRange())
        .withX(resourceMobility.getX())
        .withY(resourceMobility.getY())
        .withCompanyZoneId(resourceMobility.getCompanyZoneId())
        .withLicensePlate(resourceMobility.getLicensePlate())
        .withResourceType(resourceMobility.getResourceType())
        .withResourceImageId(resourceMobility.getResourceImageId())
        .withRealTimeData(resourceMobility.getRealTimeData())
        .withActive(resourceMobility.getActive())
        .withLastUpdate(resourceMobility.getLastUpdate())
        .build();

  }
}
