package com.meep.test.mobility.adapter.repository;

import com.meep.test.mobility.adapter.repository.vo.ResourceMobilityEntity;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceMobilityMongoRepository extends MongoRepository<ResourceMobilityEntity, String> {

  List<ResourceMobilityEntity> findResourceMobilityEntitiesByActive(Boolean active);
}
