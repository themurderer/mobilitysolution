package com.meep.test.mobility.adapter.repository;

import com.meep.test.mobility.adapter.converter.ResourceMobilityEntityToResourceMobilityConverter;
import com.meep.test.mobility.adapter.converter.ResourceMobilityToResourceMobilityEntityConverter;
import com.meep.test.mobility.adapter.repository.vo.ResourceMobilityEntity;
import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.provider.ResourceMobilityRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class ResourceMobilityRepositoryAdapter implements ResourceMobilityRepository {

  private final ResourceMobilityMongoRepository repository;

  private final ResourceMobilityEntityToResourceMobilityConverter entityToDomainConverter;

  private final ResourceMobilityToResourceMobilityEntityConverter domainToEntityConverter;

  public ResourceMobilityRepositoryAdapter(ResourceMobilityMongoRepository repository,
      ResourceMobilityEntityToResourceMobilityConverter entityToDomainConverter,
      ResourceMobilityToResourceMobilityEntityConverter domainToEntityConverter) {
    this.repository = repository;
    this.entityToDomainConverter = entityToDomainConverter;
    this.domainToEntityConverter = domainToEntityConverter;
  }

  @Override
  public List<ResourceMobility> getResourcesByActive(Boolean active) {
    List<ResourceMobilityEntity> resourceMobilityEntities = repository.findResourceMobilityEntitiesByActive(active);
    return resourceMobilityEntities.stream().map(entityToDomainConverter::convert).collect(Collectors.toList());
  }

  @Override
  public void insertOrUpdateResources(List<ResourceMobility> resourceMobilities) {
      List<ResourceMobilityEntity> resourceEntities =
          resourceMobilities.stream().map(domainToEntityConverter::convert).collect(Collectors.toList());
      repository.saveAll(resourceEntities);
  }

  @Override
  public List<ResourceMobility> getResources()  {
      List<ResourceMobilityEntity> resourceMobilityEntities = repository.findAll();
      return resourceMobilityEntities.stream().map(entityToDomainConverter::convert).collect(Collectors.toList());
  }

}
