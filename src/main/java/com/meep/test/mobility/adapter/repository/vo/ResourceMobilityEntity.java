package com.meep.test.mobility.adapter.repository.vo;

import java.time.LocalDateTime;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class ResourceMobilityEntity {

  @Id
  private String id;

  private String name;

  private Double x;

  private Double y;

  private String licensePlate;

  private Integer range;

  private Integer batteryLevel;

  private Integer helmets;

  private String model;

  private String resourceImageId;

  private Boolean realTimeData;

  private String resourceType;

  private Integer companyZoneId;

  private Boolean active;

  @LastModifiedDate
  private LocalDateTime lastUpdate;

  public static class Builder {

    private final ResourceMobilityEntity object;

    public Builder() {
      object = new ResourceMobilityEntity();
    }

    public Builder withId(String value) {
      object.id = value;
      return this;
    }

    public Builder withName(String value) {
      object.name = value;
      return this;
    }

    public Builder withX(Double value) {
      object.x = value;
      return this;
    }

    public Builder withY(Double value) {
      object.y = value;
      return this;
    }

    public Builder withLicensePlate(String value) {
      object.licensePlate = value;
      return this;
    }

    public Builder withRange(Integer value) {
      object.range = value;
      return this;
    }

    public Builder withBatteryLevel(Integer value) {
      object.batteryLevel = value;
      return this;
    }

    public Builder withHelmets(Integer value) {
      object.helmets = value;
      return this;
    }

    public Builder withModel(String value) {
      object.model = value;
      return this;
    }

    public Builder withResourceImageId(String value) {
      object.resourceImageId = value;
      return this;
    }

    public Builder withRealTimeData(Boolean value) {
      object.realTimeData = value;
      return this;
    }

    public Builder withResourceType(String value) {
      object.resourceType = value;
      return this;
    }

    public Builder withCompanyZoneId(Integer value) {
      object.companyZoneId = value;
      return this;
    }

    public Builder withActive(Boolean value) {
      object.active = value;
      return this;
    }

    public ResourceMobilityEntity build() {
      return object;
    }

  }
}
