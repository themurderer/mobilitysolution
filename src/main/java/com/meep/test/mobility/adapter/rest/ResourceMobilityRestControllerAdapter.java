package com.meep.test.mobility.adapter.rest;

import com.meep.test.mobility.adapter.converter.ResourceMobilityToResourceMobilityResponseConverter;
import com.meep.test.mobility.adapter.rest.vo.ResourceMobilityResponse;
import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.provider.ResourceMobilityRestController;
import com.meep.test.mobility.service.api.ResourceMobilityService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceMobilityRestControllerAdapter implements ResourceMobilityRestController<ResourceMobilityResponse> {

  private final ResourceMobilityService resourceMobilityService;

  private final ResourceMobilityToResourceMobilityResponseConverter domainToResponseConverter;

  public ResourceMobilityRestControllerAdapter(ResourceMobilityService resourceMobilityService,
      ResourceMobilityToResourceMobilityResponseConverter domainToResponseConverter) {
    this.resourceMobilityService = resourceMobilityService;
    this.domainToResponseConverter = domainToResponseConverter;
  }

  @Override
  public ResponseEntity<List<ResourceMobilityResponse>> getResourceFromStorage(@RequestParam(defaultValue = "true") Boolean active) {
    List<ResourceMobility> resourceMobilities = resourceMobilityService.getResourcesFromStorage(active);
    List<ResourceMobilityResponse> resourceMobilityResponses = resourceMobilities.stream().map(domainToResponseConverter::convert).collect(
        Collectors.toList());
    return new ResponseEntity<>(resourceMobilityResponses, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<List<ResourceMobilityResponse>> getResourceFromRealData() {
    List<ResourceMobility> resourceMobilities = resourceMobilityService.getResourcesFromRealData();
    List<ResourceMobilityResponse> resourceMobilityResponses = resourceMobilities.stream().map(domainToResponseConverter::convert).collect(
        Collectors.toList());
    return new ResponseEntity<>(resourceMobilityResponses, HttpStatus.OK);
  }
}
