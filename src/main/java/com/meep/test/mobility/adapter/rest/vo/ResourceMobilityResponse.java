package com.meep.test.mobility.adapter.rest.vo;

import java.time.LocalDateTime;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class ResourceMobilityResponse {

  private String id;

  private String name;

  private Double x;

  private Double y;

  private String licensePlate;

  private Integer range;

  private Integer batteryLevel;

  private Integer helmets;

  private String model;

  private String resourceImageId;

  private Boolean realTimeData;

  private String resourceType;

  private Integer companyZoneId;

  private Boolean active;

  private LocalDateTime lastUpdate;

  public static class Builder {

    private final ResourceMobilityResponse object;

    public Builder() {
      object = new ResourceMobilityResponse();
    }

    public Builder withId(String value) {
      object.id = value;
      return this;
    }

    public Builder withName(String value) {
      object.name = value;
      return this;
    }

    public Builder withX(Double value) {
      object.x = value;
      return this;
    }

    public Builder withY(Double value) {
      object.y = value;
      return this;
    }

    public Builder withLicensePlate(String value) {
      object.licensePlate = value;
      return this;
    }

    public Builder withRange(Integer value) {
      object.range = value;
      return this;
    }

    public Builder withBatteryLevel(Integer value) {
      object.batteryLevel = value;
      return this;
    }

    public Builder withHelmets(Integer value) {
      object.helmets = value;
      return this;
    }

    public Builder withModel(String value) {
      object.model = value;
      return this;
    }

    public Builder withResourceImageId(String value) {
      object.resourceImageId = value;
      return this;
    }

    public Builder withRealTimeData(Boolean value) {
      object.realTimeData = value;
      return this;
    }

    public Builder withResourceType(String value) {
      object.resourceType = value;
      return this;
    }

    public Builder withCompanyZoneId(Integer value) {
      object.companyZoneId = value;
      return this;
    }

    public Builder withActive(Boolean value) {
      object.active = value;
      return this;
    }

    public Builder withLastUpdate(LocalDateTime value) {
      object.lastUpdate = value;
      return this;
    }

    public ResourceMobilityResponse build() {
      return object;
    }

  }
}
