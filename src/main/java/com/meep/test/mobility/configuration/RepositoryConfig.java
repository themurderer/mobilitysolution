package com.meep.test.mobility.configuration;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.meep.test.mobility.adapter.repository")
public class RepositoryConfig {

  @Value("${repository.mongo.connection.url}")
  private String connectionUrl;

  @Value("${repository.mongo.credentials.username}")
  private String usernameCredential;

  @Value("${repository.mongo.credentials.password}")
  private String passwordCredential;

  @Value("${repository.mongo.database}")
  private String databaseName;

  @Bean
  public MongoClient mongoConnection() throws Exception {
    final ConnectionString connectionString = new ConnectionString(connectionUrl);
    final MongoCredential mongoCredential =
        MongoCredential.createCredential(usernameCredential, "admin", passwordCredential.toCharArray());
    final MongoClientSettings mongoClientSettings =
        MongoClientSettings.builder().applyConnectionString(connectionString).credential(mongoCredential).build();
    return MongoClients.create(mongoClientSettings);
  }

  @Bean
  public MongoTemplate mongoTemplate() throws Exception {
    return new MongoTemplate(mongoConnection(), databaseName);
  }

}
