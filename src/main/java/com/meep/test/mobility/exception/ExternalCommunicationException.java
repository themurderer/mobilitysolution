package com.meep.test.mobility.exception;

public class ExternalCommunicationException extends RuntimeException {

  private static String DEFAULT_MESSAGE = "Can't communicate with original data";

  public ExternalCommunicationException(String message, Throwable cause) {
    super(message, cause);
  }

  public ExternalCommunicationException() {
    super(DEFAULT_MESSAGE);
  }
}
