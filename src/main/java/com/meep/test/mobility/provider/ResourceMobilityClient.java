package com.meep.test.mobility.provider;

import com.meep.test.mobility.domain.ResourceMobility;
import java.util.List;

public interface ResourceMobilityClient {

  List<ResourceMobility> getResources();

}
