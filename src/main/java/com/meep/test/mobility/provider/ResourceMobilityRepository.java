package com.meep.test.mobility.provider;

import com.meep.test.mobility.domain.ResourceMobility;
import java.util.List;

public interface ResourceMobilityRepository {

  List<ResourceMobility> getResourcesByActive(Boolean active);

  void insertOrUpdateResources(List<ResourceMobility> resourceMobilities);

  List<ResourceMobility> getResources();


}
