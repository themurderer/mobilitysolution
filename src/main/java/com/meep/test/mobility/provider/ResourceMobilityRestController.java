package com.meep.test.mobility.provider;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public interface ResourceMobilityRestController<R> {

  @RequestMapping(value = {"/api/resources"}, produces = {"application/json"}, method = {RequestMethod.GET})
  default ResponseEntity<List<R>> getResourceFromStorage(@RequestParam(defaultValue = "true") Boolean active) {
    return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
  }

  @RequestMapping(value = {"/api/real/resources"}, produces = {"application/json"}, method = {RequestMethod.GET})
  default ResponseEntity<List<R>> getResourceFromRealData() {
    return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
  }
}
