package com.meep.test.mobility.service;

import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.provider.ResourceMobilityClient;
import com.meep.test.mobility.provider.ResourceMobilityRepository;
import com.meep.test.mobility.service.api.ResourceMobilityService;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ResourceMobilityServiceImpl implements ResourceMobilityService {

  private final ResourceMobilityClient clientProvider;

  private final ResourceMobilityRepository repositoryProvider;

  public ResourceMobilityServiceImpl(ResourceMobilityClient clientProvider,
      ResourceMobilityRepository repositoryProvider) {
    this.clientProvider = clientProvider;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public List<ResourceMobility> getResourcesFromStorage(Boolean active) {
    return repositoryProvider.getResourcesByActive(active);
  }

  @Override
  public List<ResourceMobility> getResourcesFromRealData() {
    return clientProvider.getResources();
  }


}
