package com.meep.test.mobility.service;

import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.provider.ResourceMobilityClient;
import com.meep.test.mobility.provider.ResourceMobilityRepository;
import com.meep.test.mobility.service.api.ScheduledTask;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ScheduledTaskImpl implements ScheduledTask {

  private final ResourceMobilityClient clientProvider;

  private final ResourceMobilityRepository repositoryProvider;

  public ScheduledTaskImpl(ResourceMobilityClient clientProvider,
      ResourceMobilityRepository repositoryProvider) {
    this.clientProvider = clientProvider;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  @Scheduled(fixedRate = 30000)
  public void updateResources() {
    log.info("Starting scheduled task who retrieves active resources");

    List<ResourceMobility> resourcesInDatabase = repositoryProvider.getResources();
    List<ResourceMobility> resourcesInStreaming = clientProvider.getResources();

    // Update actives resources
    resourcesInStreaming.forEach(r -> r.setActive(Boolean.TRUE));

    // No active resources
    List<ResourceMobility> resourcesNoActives =
        resourcesInDatabase.stream().filter(r -> r.getActive() && !resourcesInStreaming.contains(r)).collect(Collectors.toList());
    resourcesNoActives.forEach(r -> r.setActive(Boolean.FALSE));

    repositoryProvider
        .insertOrUpdateResources(Stream.concat(resourcesNoActives.stream(), resourcesInStreaming.stream()).collect(Collectors.toList()));

  }
}
