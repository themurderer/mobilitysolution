package com.meep.test.mobility.service.api;

import com.meep.test.mobility.domain.ResourceMobility;
import java.util.List;

public interface ResourceMobilityService {

  List<ResourceMobility> getResourcesFromStorage(Boolean active);

  List<ResourceMobility> getResourcesFromRealData();

}
