package com.meep.test.mobility.adapter.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.meep.test.mobility.adapter.client.vo.ResourceMobilityClientDTO;
import com.meep.test.mobility.domain.ResourceMobility;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ResourceMobilityClientDTOToResourceMobilityConverterTest {

  private static ResourceMobilityClientDTOToResourceMobilityConverter converter;

  @BeforeAll
  static void beforeAll() {
    converter = new ResourceMobilityClientDTOToResourceMobilityConverter();
  }

  @Test
  public void givenClientObjectWhenConvertIsInvokedThenResourceMobilityIsObtained() {

    // arrange
    ResourceMobilityClientDTO clientDTO =
        new ResourceMobilityClientDTO.Builder().withId("1").withModel("model1").withName("Pepe").withX(new Double("23.23"))
            .withY(new Double("23.21")).withResourceType("resourceType").withBatteryLevel(2).withCompanyZoneId(2).withLicensePlate("adas")
            .withRealTimeData(Boolean.TRUE).withHelmets(3).withRange(2).build();

    ResourceMobility target = new ResourceMobility.Builder().withId("1").withModel("model1").withName("Pepe").withX(new Double("23.23"))
        .withY(new Double("23.21")).withResourceType("resourceType").withBatteryLevel(2).withCompanyZoneId(2).withLicensePlate("adas")
        .withRealTimeData(Boolean.TRUE).withHelmets(3).withRange(2).build();

    // act
    ResourceMobility result = converter.convert(clientDTO);

    // assert
    assertNotNull(result);
    assertEquals(target, result);
  }
}