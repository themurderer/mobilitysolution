package com.meep.test.mobility.adapter.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.meep.test.mobility.adapter.converter.ResourceMobilityEntityToResourceMobilityConverter;
import com.meep.test.mobility.adapter.converter.ResourceMobilityToResourceMobilityEntityConverter;
import com.meep.test.mobility.adapter.repository.vo.ResourceMobilityEntity;
import com.meep.test.mobility.domain.ResourceMobility;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ResourceMobilityMongoRepositoryAdapterTest {

  private ResourceMobilityRepositoryAdapter repositoryAdapter;

  private ResourceMobilityEntityToResourceMobilityConverter entityToDomainConverter;

  private ResourceMobilityToResourceMobilityEntityConverter domainToEntityConverter;

  private ResourceMobilityMongoRepository repository;

  @BeforeEach
  void setUp() {
    repository = mock(ResourceMobilityMongoRepository.class);
    entityToDomainConverter = mock(ResourceMobilityEntityToResourceMobilityConverter.class);
    domainToEntityConverter = mock(ResourceMobilityToResourceMobilityEntityConverter.class);
    repositoryAdapter = new ResourceMobilityRepositoryAdapter(repository, entityToDomainConverter, domainToEntityConverter);
  }

  @Test
  public void givenActiveParameterWhenGetResourcesByActiveIsInvokedThenListIsObtained() {
    // arrange
    when(repository.findResourceMobilityEntitiesByActive(Boolean.TRUE))
        .thenReturn(List.of(new ResourceMobilityEntity(), new ResourceMobilityEntity()));

    // act
    List<ResourceMobility> result = repositoryAdapter.getResourcesByActive(Boolean.TRUE);

    // assert
    verify(repository).findResourceMobilityEntitiesByActive(Boolean.TRUE);
    verify(entityToDomainConverter, times(2)).convert(any());
    assertNotNull(result);
    assertEquals(2, result.size());

  }

  @Test
  public void givenAListOfResourcesWhenInsertOrUpdateResourcesIsInvokedThenWorks() {

    // act
    repositoryAdapter.insertOrUpdateResources(List.of(new ResourceMobility(), new ResourceMobility()));

    // assert
    verify(repository).saveAll(any());
    verify(domainToEntityConverter, times(2)).convert(any());

  }

  @Test
  public void whenGetResourcesIsInvokedThenListIsObtained() throws ExecutionException, InterruptedException {
    // arrange
    when(repository.findAll())
        .thenReturn(List.of(new ResourceMobilityEntity(), new ResourceMobilityEntity()));

    // act
    List<ResourceMobility> result = repositoryAdapter.getResources();

    // assert
    verify(repository).findAll();
    verify(entityToDomainConverter, times(2)).convert(any());
    assertNotNull(result);
    assertEquals(2, result.size());

  }
}