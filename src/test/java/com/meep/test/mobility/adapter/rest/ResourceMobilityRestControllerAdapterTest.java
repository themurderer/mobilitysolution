package com.meep.test.mobility.adapter.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.meep.test.mobility.adapter.converter.ResourceMobilityToResourceMobilityResponseConverter;
import com.meep.test.mobility.adapter.rest.vo.ResourceMobilityResponse;
import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.service.api.ResourceMobilityService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class ResourceMobilityRestControllerAdapterTest {

  private static ResourceMobilityService resourceMobilityService;

  private static ResourceMobilityToResourceMobilityResponseConverter domainToResponseConverter;

  private static ResourceMobilityRestControllerAdapter restAdapter;

  @BeforeEach
  void setUp() {
    domainToResponseConverter = mock(ResourceMobilityToResourceMobilityResponseConverter.class);
    resourceMobilityService = mock(ResourceMobilityService.class);
    restAdapter = new ResourceMobilityRestControllerAdapter(resourceMobilityService, domainToResponseConverter);

  }

  @Test
  public void givenAStatusWhenGetResourceFromStorageISInvokedAResponseEntityIsObtained() {
    // arrange
    when(resourceMobilityService.getResourcesFromStorage(Boolean.TRUE)).thenReturn(List.of(new ResourceMobility(), new ResourceMobility()));

    // act
    ResponseEntity<List<ResourceMobilityResponse>> result = restAdapter.getResourceFromStorage(Boolean.TRUE);

    // assert
    assertNotNull(result);
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertNotNull(result.getBody());
    assertEquals(2, result.getBody().size());
    verify(domainToResponseConverter, times(2)).convert(any());
  }

  @Test
  public void whenGetResourceFromRealDataISInvokedAResponseEntityIsObtained() {
    // arrange
    when(resourceMobilityService.getResourcesFromRealData()).thenReturn(List.of(new ResourceMobility(), new ResourceMobility()));

    // act
    ResponseEntity<List<ResourceMobilityResponse>> result = restAdapter.getResourceFromRealData();

    // assert
    assertNotNull(result);
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertNotNull(result.getBody());
    assertEquals(2, result.getBody().size());
    verify(domainToResponseConverter, times(2)).convert(any());
  }
}