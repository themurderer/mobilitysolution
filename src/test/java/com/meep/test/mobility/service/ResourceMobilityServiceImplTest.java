package com.meep.test.mobility.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.provider.ResourceMobilityClient;
import com.meep.test.mobility.provider.ResourceMobilityRepository;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ResourceMobilityServiceImplTest {

  private static ResourceMobilityClient clientProvider;

  private static ResourceMobilityRepository repositoryProvider;

  private static ResourceMobilityServiceImpl resourceMobilityService;

  @BeforeAll
  static void beforeAll() {
    clientProvider = mock(ResourceMobilityClient.class);
    repositoryProvider = mock(ResourceMobilityRepository.class);

    resourceMobilityService = new ResourceMobilityServiceImpl(clientProvider, repositoryProvider);
  }

  @Test
  public void givenAStatusWhenGetResourcesFromStorageThenListIsObtained() {

    // arrange
    when(repositoryProvider.getResourcesByActive(Boolean.TRUE)).thenReturn(List.of(new ResourceMobility(), new ResourceMobility()));

    // act
    List<ResourceMobility> result = resourceMobilityService.getResourcesFromStorage(Boolean.TRUE);

    // assert
    assertNotNull(result);
    assertEquals(2, result.size());
  }

  @Test
  public void whenGetResourcesFromRealDataThenListIsObtained() {

    // arrange
    when(clientProvider.getResources()).thenReturn(List.of(new ResourceMobility(), new ResourceMobility()));

    // act
    List<ResourceMobility> result = resourceMobilityService.getResourcesFromRealData();

    // assert
    assertNotNull(result);
    assertEquals(2, result.size());
  }
}