package com.meep.test.mobility.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.meep.test.mobility.domain.ResourceMobility;
import com.meep.test.mobility.provider.ResourceMobilityClient;
import com.meep.test.mobility.provider.ResourceMobilityRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ScheduledTaskImplTest {

  private ResourceMobilityClient clientProvider;

  private ResourceMobilityRepository repositoryProvider;

  private ScheduledTaskImpl scheduledTask;

  @BeforeEach
  void setUp() {
    clientProvider = mock(ResourceMobilityClient.class);
    repositoryProvider = mock(ResourceMobilityRepository.class);
    scheduledTask = new ScheduledTaskImpl(clientProvider, repositoryProvider);
  }

  @Test
  public void givenAllElementToAddInDatabase() {

    // arrange
    ResourceMobility resourceMobility1 = new ResourceMobility.Builder().withId("1").build();
    ResourceMobility resourceMobility2 = new ResourceMobility.Builder().withId("2").build();
    ResourceMobility resourceMobility3 = new ResourceMobility.Builder().withId("3").build();
    when(repositoryProvider.getResources()).thenReturn(new ArrayList<>());
    when(clientProvider.getResources()).thenReturn(List.of(resourceMobility1, resourceMobility2, resourceMobility3));

    // act
    scheduledTask.updateResources();

    // assert
    ResourceMobility resourceMobility1_ = new ResourceMobility.Builder().withId("1").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility2_ = new ResourceMobility.Builder().withId("2").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility3_ = new ResourceMobility.Builder().withId("3").withActive(Boolean.TRUE).build();
    verify(repositoryProvider).insertOrUpdateResources(List.of(resourceMobility1_, resourceMobility2_, resourceMobility3_));
  }

  @Test
  public void givenSomeElementsToPutInactiveInDatabase() {

    // arrange
    ResourceMobility resourceMobility1 = new ResourceMobility.Builder().withId("1").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility2 = new ResourceMobility.Builder().withId("2").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility3 = new ResourceMobility.Builder().withId("3").withActive(Boolean.TRUE).build();
    when(repositoryProvider.getResources()).thenReturn(List.of(resourceMobility1, resourceMobility2, resourceMobility3));
    when(clientProvider.getResources()).thenReturn(List.of(resourceMobility2, resourceMobility3));

    // act
    scheduledTask.updateResources();

    // assert
    ResourceMobility resourceMobility1_ = new ResourceMobility.Builder().withId("1").withActive(Boolean.FALSE).build();
    ResourceMobility resourceMobility2_ = new ResourceMobility.Builder().withId("2").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility3_ = new ResourceMobility.Builder().withId("3").withActive(Boolean.TRUE).build();
    verify(repositoryProvider).insertOrUpdateResources(List.of(resourceMobility1_, resourceMobility2_, resourceMobility3_));
  }

  @Test
  public void givenSomeElementsToGoBackActiveInDatabase() {
    // arrange
    ResourceMobility resourceMobility1 = new ResourceMobility.Builder().withId("1").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility2 = new ResourceMobility.Builder().withId("2").withActive(Boolean.FALSE).build();
    ResourceMobility resourceMobility3 = new ResourceMobility.Builder().withId("3").withActive(Boolean.FALSE).build();

    ResourceMobility resourceMobility1c = new ResourceMobility.Builder().withId("1").build();
    ResourceMobility resourceMobility2c = new ResourceMobility.Builder().withId("2").build();
    ResourceMobility resourceMobility3c = new ResourceMobility.Builder().withId("3").build();
    when(repositoryProvider.getResources()).thenReturn(List.of(resourceMobility1, resourceMobility2, resourceMobility3));
    when(clientProvider.getResources()).thenReturn(List.of(resourceMobility1c, resourceMobility2c, resourceMobility3c));

    // act
    scheduledTask.updateResources();

    // assert
    ResourceMobility resourceMobility1_ = new ResourceMobility.Builder().withId("1").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility2_ = new ResourceMobility.Builder().withId("2").withActive(Boolean.TRUE).build();
    ResourceMobility resourceMobility3_ = new ResourceMobility.Builder().withId("3").withActive(Boolean.TRUE).build();
    verify(repositoryProvider).insertOrUpdateResources(List.of(resourceMobility1_, resourceMobility2_, resourceMobility3_));
  }
}